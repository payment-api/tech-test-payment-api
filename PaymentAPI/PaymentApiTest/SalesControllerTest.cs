﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PaymentAPI;
using Microsoft.EntityFrameworkCore;
using Xunit;
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace PaymentApiTest
{
    public class SalesControllerTest
    {

        [Fact]
        public void GetSaleTest()
        {
            
            PaymentAPI.Controllers.SalesController salesController = new PaymentAPI.Controllers.SalesController();

            Task result = salesController.GetSale(1);

            Assert.NotNull(result);
        }
        [Fact]
        public void PutSaleTest()
        {
            PaymentAPI.Controllers.SalesController salesController = new PaymentAPI.Controllers.SalesController();

            Task result = salesController.PutSale(1);

            Assert.NotNull(result);
        }
        [Fact]
        public void CancelSaleTest()
        {
            PaymentAPI.Controllers.SalesController salesController = new PaymentAPI.Controllers.SalesController();

            Task result = salesController.CancelSale(1);

            Assert.NotNull(result);
        }
        [Fact]
        public void PostSaleTest()
        {

            var sale = new PaymentAPI.Models.Sale()
            {
                SaleId = 82,
                Date = DateTime.Now,
                ProductId = 2,
                SellerId = 2
            };

            PaymentAPI.Controllers.SalesController salesController = new PaymentAPI.Controllers.SalesController();

            Task result = salesController.PostSale(sale);

            Assert.NotNull(result);
        }
    }
}
