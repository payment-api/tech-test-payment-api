﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PaymentAPI.Data;
using PaymentAPI.Models;

namespace PaymentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public SalesController(AppDbContext context)
        {
            _context = context;
        }

        public SalesController()
        {
        }

        /// <summary>
        /// Busca Venda
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Ok</response>
        // GET: api/Sales/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sale>> GetSale(int id)
        {
            var sale = await _context.Sale.FindAsync(id);

            if (sale == null)
            {
                return NotFound();
            }

            return sale;
        }
             
        /// <summary>
        /// Atualiza Venda
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204">Não há conteúdo</response>
        // PUT: api/Sales/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSale(int id)
        {
            var sale = await _context.Sale.FindAsync(id);

            if (SaleExists(id))
            {
                return BadRequest();
            }

            switch (sale.SaleStatusId)
            {
                case Sale.SaleStatus.AguardandoPagamento
                    :
                    sale.SaleStatusId = Sale.SaleStatus.PagamentoAprovado;
                    break;
                case Sale.SaleStatus.PagamentoAprovado
                    :
                    sale.SaleStatusId = Sale.SaleStatus.EnviadoParaTransportadora;
                    break;
                case Sale.SaleStatus.EnviadoParaTransportadora
                    :
                    sale.SaleStatusId = Sale.SaleStatus.Entregue;
                    break;
            }

            _context.Entry(sale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(sale);
        }

        /// <summary>
        /// Cancelar Venda
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204">Não há conteúdo</response>
        // PUT: api/Sales/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> CancelSale(int id)
        {
            var sale = await _context.Sale.FindAsync(id);

            if (SaleExists(id))
            {
                if (sale.SaleStatusId != Sale.SaleStatus.AguardandoPagamento && sale.SaleStatusId != Sale.SaleStatus.PagamentoAprovado)
                    {
                    return BadRequest();
                    }
            }

            sale.SaleStatusId = Sale.SaleStatus.Cancelada;

            _context.Entry(sale).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SaleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(sale);
        }

        /// <summary>
        /// Registra Venda
        /// </summary>
        /// <param name="sale"></param>
        /// <returns></returns>
        /// <response code="201">Criado</response>
        /// <response code="200">Sucesso</response>
        // POST: api/Sales
        [HttpPost]
        public async Task<ActionResult<Sale>> PostSale(Sale sale)
        {
            _context.Sale.Add(sale);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSale", new { id = sale.SaleId }, sale);
        }

        private bool SaleExists(int id)
        {
            return _context.Sale.Any(e => e.SaleId == id);
        }
    }
}
