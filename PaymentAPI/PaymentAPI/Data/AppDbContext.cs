﻿using Microsoft.EntityFrameworkCore;
using System;
using PaymentAPI.Models;
using static PaymentAPI.Models.Sale;

namespace PaymentAPI.Data
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Seller>().HasData(
                new Seller { SellerId = 1, Cpf = "651651616", Name = "João", Email = "jao@email.com", Phone = "1234567" },
                new Seller { SellerId = 2, Cpf = "561651651", Name = "Carlos", Email = "carlos@email.com", Phone = "4165114" },
                new Seller { SellerId = 3, Cpf = "123456789", Name = "Pedro", Email = "pedro@email.com", Phone = "6541641" });

            modelBuilder.Entity<Product>().HasData(
               new Product { ProductId = 1, Name = "Caderno", Price = 9.10 },
               new Product { ProductId = 2, Name = "Lápis", Price = 0.80 },
               new Product { ProductId = 3, Name = "Borracha", Price = 2.00 });

            modelBuilder.Entity<Request>().HasData(
              new Request { RequestId = 1, SellerId = 1, ProductId = 2 }
              ) ;
            

            modelBuilder.Entity<Sale>(s => {
                s.HasData(
                   new Sale { SaleId = 1, Date = DateTime.Today, ProductId = 2, RequestId = 1, SaleStatusId = SaleStatus.AguardandoPagamento, SellerId = 1 });

                s.Property(s => s.SellerId).HasColumnName("SellerId");
               });
            
        }

        public DbSet<Seller> Sellers { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Sale> Sale { get; set; }

        public DbSet<Request> Request { get; set; }
    }
}
