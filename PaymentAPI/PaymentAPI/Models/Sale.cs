﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentAPI.Models
{
    public class Sale 
    {
        public enum SaleStatus : int
        {
            AguardandoPagamento = 0,
            PagamentoAprovado = 1,
            EnviadoParaTransportadora = 2,
            Cancelada = 3,
            Entregue = 4
        };

        public int SaleId { get; set; }
        public int? SellerId { get; set; }
        public DateTime Date { get; set; }
        public int RequestId { get; set; }
        public int ProductId { get; set; }
        public SaleStatus SaleStatusId { get; set; }

        public virtual Seller? Seller { get; set; }
}
}
