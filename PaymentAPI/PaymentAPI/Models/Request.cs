﻿
namespace PaymentAPI.Models
{
    public class Request
    {
        public int RequestId { get; set; }
        public Seller Seller { get; set; }
        public int SellerId { get; set; }
        public Product Product { get; set; }
        public int ProductId { get; set; }

    }
}
